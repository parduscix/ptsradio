package droidtr.ptsradyo;

import android.app.*;
import android.content.*;
import android.media.*;
import android.os.*;
import android.widget.*;
import java.io.*;

public class RadioService extends Service {
	
	String link = "http://185.198.199.78:8080/pts.ogg";
	MediaPlayer mp;

	@Override
	public IBinder onBind(Intent p1){
		return null;
	}

	@Override
	public void onStart(Intent intent, int startId){
		try { 
			bg();
		} catch(Exception e){
			super.onDestroy();
		}
		super.onStart(intent, startId);
	}
	
	public void bg() throws IOException {
		mp = new MediaPlayer();
		mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
				@Override
				public void onPrepared(MediaPlayer p1){
					p1.start();
				}
			});

		mp.setOnErrorListener(new MediaPlayer.OnErrorListener(){
				@Override
				public boolean onError(MediaPlayer p1, int p2, int p3){
					Toast.makeText(RadioService.this,"An error occurred, reset process...",Toast.LENGTH_SHORT).show();
					try {
						mp.setDataSource(link);
						mp.prepareAsync();
					} catch(Exception e){}
					return false;
				}
			});

		mp.setDataSource(link);
		mp.prepareAsync();
		sendNotification();
	}
	
	boolean stop = false;
	
	@Override
	public void onDestroy(){
		mp.stop();
		if(!stop) startService(new Intent(RadioService.this,RadioService.class));
		else System.exit(0);
		super.onDestroy();
	}
	
	public void sendNotification(){
		RemoteViews rv = new RemoteViews(getPackageName(), R.layout.status);
		Intent i2 = new Intent("music.recv.STOP");
		PendingIntent pi2 = PendingIntent.getBroadcast(this,0,i2,0);
		Notification n = new Notification();
		n.contentIntent = pi2;
		n.contentView = rv;
		n.icon = R.drawable.ic_play_dark;
		n.tickerText = "Playing...";
		n.extras.putString(Notification.EXTRA_TITLE,"PTSRadio is playing...");
		startForeground(999,n);
		registerReceiver(new MusicRecv(),new IntentFilter("music.recv.STOP"));
	}
	
	public class MusicRecv extends BroadcastReceiver{
		@Override
		public void onReceive(Context p1, Intent p2){
			stop = true;
			stopForeground(true);
			stopSelf();
		}
	}
	
}
